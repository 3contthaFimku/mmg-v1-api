<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|   
| https://laravel.com/docs/10.x/routing
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// https://blog.quickadminpanel.com/laravel-api-documentation-with-openapiswagger/
Route::group([
    'prefix' => 'v1', 
    'as' => 'api.', 
    'namespace' => 'App\Http\Controllers\Api', 
    'middleware' => ['auth:sanctum']
  ], function () {
      Route::apiResource('plant', 'PlantApiController');
      Route::apiResource('plant-definition', 'PlantDefinitionApiController');
      Route::apiResource('plant-family', 'PlantFamilyApiController');
      Route::apiResource('task', 'TaskApiController');
      Route::apiResource('task-definition', 'TaskDefinitionApiController');
      Route::apiResource('task-definition-type', 'TaskDefinitionTypeApiController');
      Route::apiResource('zone', 'ZoneApiController');
  });