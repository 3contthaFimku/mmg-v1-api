<?php

namespace App\Http\Controllers;

use App\Http\Controllers\TestController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Homepage', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('home');

/*
Route::get('/login', function () {
    return Inertia::render('Login', [

    ]);
})->name('login');


Route::get('/register', function () {
    return Inertia::render('Register', [

    ]);
})->name('register');
*/

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {  

        $user = User::findOrFail(Auth::id());
        return Inertia::render('Dashboard', ['user' =>  $user]);
    })->name('dashboard');

    Route::get('/homepage', [HomePageController::class, 'index'])->name('homepage');
});


Route::put('/zones/{id}', [HomePageController::class, 'update']);
