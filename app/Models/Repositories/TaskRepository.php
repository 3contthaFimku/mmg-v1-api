<?php
namespace App\Models\Repositories;

use App\Models\Plant;
use App\Models\Task;

class TaskRepository extends BaseRepository
{
    public function __construct(Task $model)
    {
        parent::__construct($model);
    }

    public function getByPlant(Plant $plant) {
        return $this->model->where('plant_id', $plant->id)->get();
    }
}
