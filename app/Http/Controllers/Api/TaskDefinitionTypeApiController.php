<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreTaskDefinitionTypeRequest;
use App\Http\Requests\Api\UpdateTaskDefinitionTypeRequest;
use App\Http\Resources\TaskDefinitionTypeResource;
use App\Http\Resources\TaskDefinitionTypeCollection;
use App\Models\TaskDefinitionType;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class TaskDefinitionTypeApiController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/task-definition-type",
     *      operationId="listTaskDefinitionType",
     *      tags={"TaskDefinitionType"},
     *      summary="List all task definition types",
     *      description="Returns task definition types data",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"data": "[]", "links": {"self": "link-value"}}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     * )
     */
    public function index()
    {
        return new TaskDefinitionTypeCollection(TaskDefinitionType::all());
    }

    /**
     * @OA\Post(
     *      path="/api/v1/task-definition-type",
     *      operationId="storeTaskDefinitionType",
     *      tags={"TaskDefinitionType"},
     *      summary="Store new task definition type",
     *      description="Returns task definition type data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name"},
     *              @OA\Property(property="name", type="string", example="Sow")
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreTaskDefinitionTypeRequest $request)
    {
        $type = TaskDefinitionType::create($request->all());

        return (new TaskDefinitionTypeResource($type))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }
    /**
     * @OA\Get(
     *      path="/api/v1/task-definition-type/{id}",
     *      operationId="getTaskDefinitionTypeById",
     *      tags={"TaskDefinitionType"},
     *      summary="Get task definition type information",
     *      description="Returns task definition type data",
     *      @OA\Parameter(
     *          name="id",
     *          description="task definition type id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 example={"id": 1, "name": "Sow"}
     *             )
     *         )
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(string $id)
    {
        Log::debug("Show task definition type $id");
        $type = TaskDefinitionType::findOrFail($id);
        return (new TaskDefinitionTypeResource($type))->response();
    }

    /**
     * @OA\Put(
     *      path="/api/v1/task-definition-type/{id}",
     *      operationId="updateTaskDefinitionType",
     *      tags={"TaskDefinitionType"},
     *      summary="Update existing task definition type",
     *      description="Returns updated task definition type data",
     *      @OA\Parameter(
     *          name="id",
     *          description="task definition type id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              required={"name"},
     *              @OA\Property(property="name", type="string", example="Sow"),
     *          ),  
     *     ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(string $id, UpdateTaskDefinitionTypeRequest $request) // , string $id)
    {
        Log::debug("Udpate task definition type $id");
        $type = TaskDefinitionType::findOrFail($id);
        $type->update($request->all());

        return (new TaskDefinitionTypeResource($type))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/api/v1/task-definition-type/{id}",
     *      operationId="deleteTaskDefinitionType",
     *      tags={"TaskDefinitionType"},
     *      summary="Delete existing task definition type",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="task definition type id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(String $id)
    {
        $type = TaskDefinitionType::find($id);
        $http_code =  Response::HTTP_NOT_FOUND;
        if(!empty($type)) {
            $type->delete();
            $http_code =  Response::HTTP_NO_CONTENT;
        }
        return response(null, $http_code);
    }
}