<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ZoneRequest;

class StoreZoneRequest extends ZoneRequest
{
    use ApiRequestTrait;
}
